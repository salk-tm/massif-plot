from argparse import ArgumentParser
import pandas as pd
import seaborn as sns

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('massif_out')
    parser.add_argument('output')
    return parser.parse_args()

def gen_massif_vals(f):
    for line in f:
        if line.startswith('time='):
            time = int(line.rstrip().split('=')[1])
        if line.startswith('mem_heap_B='):
            mem_heap_b = int(line.rstrip().split('=')[1])
            yield time, mem_heap_b

def main():
    args = parse_arguments()
    with open(args.massif_out, 'r') as f:
        df = pd.DataFrame(gen_massif_vals(f), columns=('time', 'mem_heap_B'))
    ax = sns.lineplot(data=df, x='time', y='mem_heap_B')
    fig = ax.get_figure()
    fig.tight_layout()
    fig.savefig(args.output)

if __name__ == '__main__':
    main()
